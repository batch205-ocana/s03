class Person{
    constructor(name,age,nationality,address){
        this.name = name;
        this.age = age;
        this.nationality = nationality;
        this.address = address;
        if(typeof age !== "number"){
            this.age = undefined;
        } else {
            this.age = age;
        }
    }
    greet(){
        console.log(`Hello! Good Morning!`);
        return this;
    };
    introduce(){
        console.log(`Hi! My name is ${this.name}`);
        return this;
    };
    changeAddress(newAddress){
        this.address = newAddress;
        return this;
    }
}

let person1 = new Person('Jess',"18",'German','Somewhere, Germany');
console.log(person1);
let person2 = new Person('Jack',32,'Filipino','IDK, Philippines');
console.log(person2);

person1.greet().introduce();
person2.greet().introduce();

class Student{
    constructor(name,email,grades){
        this.name = name;
        this.email = email;
        this.grades = grades;
        this.grades.forEach(item => {
            if(typeof item !== "number"){
                this.grades = undefined;
            }
        });
        this.average = undefined;
        this.isPassed = undefined;
        this.isPassedWithHonors = undefined;
    }
    login(){
        console.log(`${this.email} has logged in`);
        return this;

    };
    logout(){
        console.log(`${this.email} has logged out`);
        return this;
    };
    listGrades(){
        this.grades.forEach(grade => {
            console.log(grade);
        })
        return this;
    };
    computeAve(){
        let sum = this.grades.reduce((accumulator,num) => {
            return accumulator += num;
        })
        this.average = sum/4;
        return this;
    };
    willPass(){
        this.isPassed =  Math.round(this.computeAve().average) >= 85 ? true : false;
        return this;
    };
    willPassWithHonors(){
        this.isPassedWithHonors = (this.willPass().isPassed && Math.round(this.computeAve().average) >= 90) ? true : this.willPass().isPassed ? false : undefined;
        return this;
    }

}

let student1 = new Student('John','john@mail.com',[89, 84, 78, 88])
let student2 = new Student('Joe','joe@mail.com',[78, 82, 79, 85])
let student3 = new Student('Jane','jane@mail.com',[87, 89, 91, 93])
let student4 = new Student('Jessie','jessie@mail.com',[91, 89, 92, 93])

console.log(student1.computeAve().willPass().willPassWithHonors());
console.log(student2.computeAve().willPass().willPassWithHonors());
console.log(student3.computeAve().willPass().willPassWithHonors());
console.log(student4.computeAve().willPass().willPassWithHonors());